# Plotting up the CTD data
# Leg3_depth_plot.R is a standalone plot showing the date and stations
# CTD_Plots.R is kind of rough - the first part reads in the files, then a 
# loop writes out a bunch of .pngs
# After that are various plots that become interactive with the "plotly" 
# package to make it easier to explore / q-c the data

library(dplyr)
library(ggplot2)
library(cowplot)
library(stringr)
library(plotly)
theme_set(theme_bw())

filepath = "C:/Temp/csvs"

csvs = list.files(filepath, recursive = T, pattern = ".csv", full.names = T)
# Remove unwanted files from list
# csvs = csvs[-which(!is.na(str_match(csvs, pattern = "sites-casts")))]
# Get filenames to convert to .png names
csvs_short = list.files(filepath, recursive = T, pattern = ".csv", full.names = F)
# csvs_short = csvs_short[-which(!is.na(str_match(csvs_short, pattern = "sites-casts")))]
csvs_short = str_remove(str_remove(csvs_short, "/"), "/")
csvs_short = tools::file_path_sans_ext(csvs_short)
# csvs_short = substr(csvs_short, 6, nchar(csvs_short)-4)

# All CTD data ####

for(i in 1:length(csvs)) {
  file1 = read.csv(csvs[i])
  colnames(file1)[1] = "Time"
  which(diff(file1$bottles.fired) == 1)
  
  a = file1 %>% 
    filter(pressure_db > 0) %>% 
    ggplot(aes(x = fluorescence_mg.m.3, y = pressure_db*-1)) +
    #geom_point(aes(colour = as.factor(Bottles.fired))) +
    geom_point(aes(colour = Time/60)) +
    ylab("Pressure") +
    labs(colour = "Time elapsed (minutes)") +
    scale_colour_viridis_c() + ggtitle("Fluorescence (mg m^-3)")+
    theme(legend.position = "none")
  
  b = file1 %>% 
    filter(pressure_db > 0) %>% 
    ggplot(aes(x = temperature_ITS.90_degC, y = pressure_db*-1)) +
    #geom_point(aes(colour = as.factor(Bottles.fired))) +
    geom_point(aes(colour = Time/60)) +
    ylab("Pressure") +
    labs(colour = "Time elapsed (minutes)") +
    scale_colour_viridis_c() + ggtitle("Temperature")+
    theme(legend.position = "none")
  
  c = file1 %>% 
    filter(pressure_db > 0) %>% 
    ggplot(aes(x = salinity_psu, y = pressure_db*-1)) +
    #geom_point(aes(colour = as.factor(Bottles.fired))) +
    geom_point(aes(colour = Time/60)) +
    ylab("Pressure") +
    labs(colour = "Time elapsed (minutes)") +
    scale_colour_viridis_c() + ggtitle("Salinity") +
    theme(legend.position = "none")
  
  d = file1 %>% 
    filter(pressure_db > 0) %>% 
    ggplot(aes(x = oxygen_ml.l, y = pressure_db*-1)) +
    #geom_point(aes(colour = as.factor(Bottles.fired))) +
    geom_point(aes(colour = Time/60)) +
    ylab("Pressure") +
    labs(colour = "Time elapsed (minutes)") +
    scale_colour_viridis_c() + ggtitle("Oxygen")+
    theme(legend.position = "bottom")
  
  e = file1 %>% 
    filter(pressure_db > 0) %>% 
    ggplot(aes(x = conductivity_S.m, y = pressure_db*-1)) +
    #geom_point(aes(colour = as.factor(Bottles.fired))) +
    geom_point(aes(colour = Time/60)) +
    ylab("Pressure") +
    labs(colour = "Time elapsed (minutes)") +
    scale_colour_viridis_c() + ggtitle("Conductivity")+
    theme(legend.position = "bottom")
  
  plot_grid(a,b,c,d,e, nrow = 2)
  
  ggsave(filename = paste0("Plot_",csvs_short[i],".png"), device = "png", scale = 1.5)
}

# All sites plotted on top of each other ####

# Good are N1, N2, N4, N7, N8, N15
idx_good = c(1,2,5,6,7,9,10,11,12,14,16,18,19,20,21,22,23,26)
good_ctds = csvs[idx_good]
good_ctdnames = csvs_short[idx_good]
ctddata = list()
for(j in 1:length(good_ctds)) {
  ctd = read.csv(good_ctds[j])  
  colnames(ctd)[1] = "Time"
  ctd$Station = good_ctdnames[j]
  ctd = ctd %>% mutate(DIFF = c(NA,diff(pressure_db)),
                               Direction = case_when(DIFF < -0.03 ~ "up",
                                                     DIFF >= -0.03 & DIFF <=0.03 ~ "stationary",
                                                     DIFF > 0.03 ~ "down"))
  
  ctddata[[j]] = ctd
}
ctddata = do.call("rbind", ctddata)
ctddata$Station = str_remove(ctddata$Station, "-")
ctddata$Station = substr(ctddata$Station, 1,3)
ctddata$Sector = str_to_upper(substr(ctddata$Station, 1, 1))

write.csv(x = ctddata, file = "CTD_Data_assembled.csv", quote = F, row.names = F)

# Checking bottle firing

ctddata %>% 
  filter(pressure_db > 0,
         Direction == "down") %>%
  ggplot(aes(x = oxygen_ml.l, y = pressure_db*-1)) +
  geom_point(aes(colour = Sector))
plotly::ggplotly(t)

ctddata %>% 
  filter(pressure_db > 0) %>% 
  ggplot(aes(x = Time, y = pressure_db*-1)) +
  geom_jitter(aes(colour = as.factor(bottles.fired))) +
  facet_wrap(~Station) -> t
plotly::ggplotly(t)


# All stations plotted on top of each other
ctddata %>% 
  filter(pressure_db > 0) %>% 
  ggplot(aes(x = salinity_psu, y = pressure_db*-1, label = Time)) +
  geom_point(aes(colour = Station)) -> t
plotly::ggplotly(t)

ctddata %>% 
  filter(Direction == "down") %>% 
  ggplot(aes(x = conductivity_S.m, y = pressure_db*-1)) +
  geom_point(aes(colour = Station)) +
  geom_hline(yintercept = 0, colour = "red") +
  geom_hline(yintercept = c(-0.03, 0.03), colour = "black", linetype = "dashed")

# T-S Diagram

ctddata %>% 
  filter(Direction == "down", pressure_db > 0) %>% 
  ggplot(aes(x = salinity_psu, y = temperature_ITS.90_degC)) +
  geom_point(aes(colour = Station)) 
  scale_colour_viridis_c(trans = "log10")

# Checking out the before and after fixing the ctd

before = csvs[2]
after = csvs[1]
s13_bef = read.csv(before)
s13_aft = read.csv(after)
colnames(s13_bef)[1] = "Time"
colnames(s13_aft)[1] = "Time"
plot(s13_bef$Temperature,s13_aft$Temperature)

s13_bef$ID = "before"
s13_aft$ID = "after"
s13 = rbind(s13_bef, s13_aft)

s13 %>% 
  ggplot(aes(x = Oxygen, y = pressure_db*-1)) +
  geom_point(aes(colour = as.factor(ID)))

test = full_join(s13_bef, s13_aft, by = "Pressure")


timeS: Time, Elapsed [seconds]
prdM: Pressure, Strain Gauge [db]
tv290C: Temperature [ITS-90, deg C]
c0S/m: Conductivity [S/m]
sal00: Salinity, Practical [PSU]
flECO-AFL: Fluorescence, WET Labs ECO-AFL/FL [mg/m^3]
sbeox0ML/L: Oxygen, SBE 43 [ml/l]
nbf: Bottles Fired
flag:  0.000e+00
